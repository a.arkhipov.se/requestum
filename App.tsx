/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import {Provider} from 'react-redux';
import {persistor, store} from './src/store';
import {PersistGate} from 'redux-persist/integration/react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import RootNavigation from './src/navigation/RootNavigation';
import changeNavigationBarColor from 'react-native-navigation-bar-color';
import {ThemeProvider} from 'styled-components/native';
import {ThemeProvider as ThemeContext} from './src/assets/ThemeProvider';
import {styledTheme} from './src/assets';
import {NavigationContainer} from '@react-navigation/native';
import {theme} from './src/assets/ThemeContext';
import {QueryClient, QueryClientProvider} from 'react-query';
import {LogBox} from 'react-native';

LogBox.ignoreLogs(['ReactImageView: Image source "null" doesn\'t exist']); // Ignore log notification by message

function App(): React.JSX.Element {
  changeNavigationBarColor('#1e1e1e', false, true);

  const queryClient = new QueryClient();

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <SafeAreaProvider>
          <ThemeContext>
            <ThemeProvider theme={styledTheme}>
              <QueryClientProvider client={queryClient}>
                <NavigationContainer theme={theme}>
                  <RootNavigation />
                </NavigationContainer>
              </QueryClientProvider>
            </ThemeProvider>
          </ThemeContext>
        </SafeAreaProvider>
      </PersistGate>
    </Provider>
  );
}

export default App;
