import React, {ForwardedRef, forwardRef, useImperativeHandle} from 'react';
import {ButtonContainer, ButtonTitle} from './styled';
import {TCButtonPropTypes} from './types';
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withRepeat,
  withSequence,
  withTiming,
} from 'react-native-reanimated';
import {Vibration, View} from 'react-native';

const StyledButton = forwardRef(function StyledButton(
  {title, onPress, disabled = false, style, color, textColor, type = 'solid'}: TCButtonPropTypes,
  ref: ForwardedRef<View>,
) {
  const offset = useSharedValue(0);

  const animatedStyle = useAnimatedStyle(() => ({
    transform: [{translateX: offset.value}],
  }));

  const OFFSET = 1;
  const TIME = 30;

  useImperativeHandle(ref, () => ({
    errorHandler: () => {
      Vibration.vibrate(50);
      offset.value = withSequence(
        withTiming(-OFFSET, {duration: TIME / 2}),
        withRepeat(withTiming(OFFSET, {duration: TIME}), 2, true),
        withTiming(0, {duration: TIME / 2}),
      );
    },
  }));

  return (
    <Animated.View ref={ref} style={animatedStyle}>
      <ButtonContainer
        type={type}
        style={style}
        activeOpacity={0.7}
        disabled={disabled}
        color={color}
        onPress={onPress}>
        <ButtonTitle textColor={textColor} type={type}>
          {title}
        </ButtonTitle>
      </ButtonContainer>
    </Animated.View>
  );
});

export default StyledButton;
