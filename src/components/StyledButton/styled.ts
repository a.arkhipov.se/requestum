import styled from 'styled-components/native';
import {TCButton, TCButtonText} from './types';

export const ButtonContainer = styled.TouchableOpacity<TCButton>`
  height: 50px;
  border-radius: 8px;
  background-color: ${props => (props.type === 'solid' ? props.color : 'transparent')};
  justify-content: center;
  align-items: center;
  align-self: ${props => (props.type === 'solid' ? 'stretch' : 'auto')};
  opacity: ${props => (props.disabled ? 0.5 : 1)};
  shadow-color: ${props => (props.type === 'solid' ? props.color : 'transparent')};
  shadow-offset: 10px 10px;
  shadow-opacity: 0.5;
  shadow-radius: 10px;
  elevation: 10;
`;

export const ButtonTitle = styled.Text<TCButtonText>`
  color: ${props => props.textColor || 'white'};
  font-family: ${props => (props.type === 'solid' ? props.theme.fonts.montserrat : props.theme.fonts.montserratLight)};
  font-size: ${props => (props.type === 'solid' ? `${props.theme.size[16]}px` : `${props.theme.size[14]}px`)};
  font-weight: 700;
  line-height: ${props => `${props.theme.size[18]}px`};
  text-decoration: ${props => (props.type === 'text' ? 'underline' : undefined)};
`;
