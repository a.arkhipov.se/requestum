import {StyleProp, ViewStyle} from 'react-native';

type ButtonType = 'solid' | 'text';

export type TCButton = {
  color?: string;
  type: ButtonType;
};

export type TCButtonText = {
  textColor?: string;
  type?: ButtonType;
};

export type TCButtonPropTypes = {
  type: ButtonType;
  title: string;
  onPress?: () => void;
  onError?: () => void;
  color?: string;
  textColor?: string;
  disabled?: boolean;
  style?: StyleProp<ViewStyle>;
};
