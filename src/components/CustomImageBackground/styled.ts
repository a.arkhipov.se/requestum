import styled from 'styled-components/native';
import {TCImageBackground} from './types';

export const StyledImageBackground = styled.ImageBackground<TCImageBackground>`
  flex: 1;
  flex-grow: 1;
  align-self: stretch;
  background-color: ${props => props.theme.darkGray};
`;
