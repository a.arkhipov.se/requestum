import React, {FC} from 'react';
import {StyledImageBackground} from './styled';
import {TCustomImageBackground} from './types';

const CustomImageBackground: FC<TCustomImageBackground> = ({source, children}) => {
  // const link = '../../assets/images/bg.png';
  return <StyledImageBackground source={source}>{children}</StyledImageBackground>;
};

export default CustomImageBackground;

// source={require(link)
