import {ImageSourcePropType} from 'react-native';

export type TCustomImageBackground = {
  source: ImageSourcePropType | undefined;
  children: React.ReactNode;
};

export type TCButtonText = {
  textColor: string;
};

export type TCImageBackground = {};
