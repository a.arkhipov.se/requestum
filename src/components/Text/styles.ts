import {StyleSheet} from 'react-native';
import {Theme} from '@react-navigation/native';

const styles = (colors: Theme['colors']) =>
  StyleSheet.create({
    container: {
      width: '100%',
      height: 40,
      borderRadius: 20,
      alignSelf: 'center',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: colors.primary,
    },
    disabled: {
      backgroundColor: colors.border,
    },
    title: {
      letterSpacing: 0,
      textAlign: 'left',
    },
  });

export default styles;
