import React, {memo, useContext, useMemo} from 'react';
import {StyleSheet, View, Text as NativeText, TextStyle, ViewStyle} from 'react-native';
import {ThemeContext, TypographyKeys} from '../../assets/ThemeContext';

type Props = {
  text: string | undefined;
  style?: TextStyle & ViewStyle;
  textStyle?: TextStyle;
  textColor?: string;
  typography?: TypographyKeys | undefined;
  align?: TextStyle['textAlign'];
};

interface ITextStyle extends TextStyle {
  typography?: TextStyle | undefined;
  textColor?: string;
  align?: TextStyle['textAlign'];
}

const Text: React.FC<Props> = memo(({text, style, align, typography = undefined, textColor = undefined}) => {
  const {typography: typo} = useContext(ThemeContext);

  const {
    color,
    fontFamily,
    fontSize,
    fontStyle,
    fontWeight,
    letterSpacing,
    lineHeight,
    textAlign,
    textDecorationLine,
    textDecorationStyle,
    textDecorationColor,
    textShadowColor,
    textShadowOffset,
    textShadowRadius,
    textTransform,
    ...props
  } = style || {};

  const styles = useMemo(
    () =>
      createStyles({
        color,
        fontFamily,
        fontSize,
        fontStyle,
        fontWeight,
        letterSpacing,
        lineHeight,
        textAlign,
        textDecorationLine,
        textDecorationStyle,
        textDecorationColor,
        textShadowColor,
        textShadowOffset,
        textShadowRadius,
        textTransform,
        textColor,
        align,
        typography: typography ? typo[typography] : undefined,
      }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [style, align, typography, textColor],
  );

  return (
    <View style={{...styles.textWrapper, ...props}}>
      <NativeText style={styles.textStyle}>{text}</NativeText>
    </View>
  );
});

const createStyles = ({
  color,
  fontFamily,
  fontSize,
  fontStyle,
  fontWeight,
  letterSpacing,
  lineHeight,
  textAlign,
  textDecorationLine,
  textDecorationStyle,
  textDecorationColor,
  textShadowColor,
  textShadowOffset,
  textShadowRadius,
  textTransform,
  textColor,
  align,
  typography,
}: ITextStyle) =>
  StyleSheet.create({
    textWrapper: {
      justifyContent: 'center',
      alignItems: 'flex-start',
    },
    textStyle: {
      color: textColor || color || undefined,
      fontFamily: fontFamily || typography?.fontFamily || undefined,
      fontSize: fontSize || typography?.fontSize || undefined,
      fontStyle: fontStyle || typography?.fontStyle || undefined,
      fontWeight: fontWeight || typography?.fontWeight || undefined,
      letterSpacing: letterSpacing || typography?.letterSpacing || undefined,
      lineHeight: lineHeight || typography?.lineHeight || undefined,
      textAlign: align || textAlign || undefined,
      textDecorationLine: textDecorationLine || undefined,
      textDecorationStyle: textDecorationStyle || undefined,
      textDecorationColor: textDecorationColor || undefined,
      textShadowColor: textShadowColor || undefined,
      textShadowOffset: textShadowOffset || undefined,
      textShadowRadius: textShadowRadius || undefined,
      textTransform: textTransform || typography?.textTransform || undefined,
    },
  });

export default memo(Text);
