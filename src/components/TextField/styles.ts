import {StyleSheet} from 'react-native';
import {Theme} from '../../assets/ThemeContext';

const styles = (theme: Theme) =>
  StyleSheet.create({
    container: {
      width: '100%',
    },
    innerContainer: {
      height: 56,
      paddingHorizontal: 12,
      flexGrow: 1,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      borderWidth: 2,
      borderRadius: 5,
      borderColor: theme.colors.darkGray,
      backgroundColor: 'transparent',
    },
    innerContainerError: {
      borderColor: theme.colors.primaryRed,
    },
    leadingIconContainer: {
      justifyContent: 'center',
      alignItems: 'center',
      width: 50,
      paddingLeft: 16,
      paddingRight: 14,
      height: '100%',
    },
    innerContainerWithIcons: {
      paddingHorizontal: 0,
    },
    innerContainerPassword: {
      paddingRight: 0,
    },
    labelWithIcons: {
      left: 48,
    },
    innerContainerPaddingLeft: {
      paddingLeft: 16,
    },
    label: {
      backgroundColor: 'transparent',
      position: 'absolute',
      left: 12,
      zIndex: -300,
      paddingHorizontal: 6,
      fontSize: 16,
    },
    labelError: {
      color: theme.colors.lightGray,
    },
    textInput: {
      marginTop: 16,
      flexGrow: 1,
      flexShrink: 1,
      color: 'white',
      justifyContent: 'center',
      textAlign: 'left',
      textAlignVertical: 'center',
      fontFamily: 'Montserrat',
      fontWeight: '700',
    },
    focusedContainer: {
      borderWidth: 2,
      borderColor: theme.colors.primaryRed,
    },
    trailingIconContainer: {
      width: 48,
      paddingHorizontal: 16,
      height: '100%',
      justifyContent: 'center',
      alignItems: 'center',
    },
  });

export default styles;
