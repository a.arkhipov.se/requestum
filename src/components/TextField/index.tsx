import React, {useState, useMemo, useCallback, forwardRef, useRef} from 'react';
import {View, TextInput, ViewStyle, TextStyle, TouchableWithoutFeedback, TouchableOpacity} from 'react-native';

import textFieldStyles from './styles';
import Animated, {
  Easing,
  interpolateColor,
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';
import Assets, {IconType} from '../../assets/svg';
import {useTheme} from '@react-navigation/native';
import ErrorText from '../ErrorText';

interface Props {
  value: string;
  onChangeText: (text: string) => void;
  label?: string;
  returnKetType?: 'done' | 'go' | 'next' | 'search' | 'send';
  maxLength?: number;
  error: string | undefined;
  isPassword?: boolean;
  leadingIcon?: null | IconType;
  trailingIcon?: null | IconType;
  onTrailingIconPress?: () => void;
  onFocus?: () => void;
  onBlur?: () => void;
  onSubmit?: () => void;
  containerStyle?: ViewStyle;
  textInputStyle?: ViewStyle | TextStyle;

  leadingIconContainerStyle?: ViewStyle;
  trailingIconIconContainerStyle?: ViewStyle;
}

const TextField = forwardRef(function TextField(
  {
    value,
    onChangeText,
    label = 'Label',
    isPassword = false,
    maxLength = 52,
    returnKetType = 'done',
    error,
    leadingIcon = null,
    trailingIcon = null,
    onFocus = () => {},
    onBlur = () => {},
    onSubmit = () => {},
    onTrailingIconPress = () => {},
    containerStyle = {},
    textInputStyle = {},
    leadingIconContainerStyle = {},
    trailingIconIconContainerStyle = {},
  }: Props,
  ref: React.ForwardedRef<TextInput>,
) {
  const textInputRef = useRef<TextInput | null>(null);

  const [isFocused, setIsFocused] = useState(false);
  const [isPasswordVisible, setIsPasswordVisible] = useState(false);

  const theme = useTheme();

  const translateY = useSharedValue(0);
  const scale = useSharedValue(1);
  const translateX = useSharedValue(0);
  const colorAnimation = useSharedValue(0);

  const DURATION_TIME = 200;

  const labelAnimationProperties = useMemo(() => {
    return {duration: DURATION_TIME, easing: Easing.linear};
  }, [DURATION_TIME]);

  const styles = useMemo(() => textFieldStyles(theme), [theme]);

  const setRef = () => {
    if (textInputRef.current != null) {
      textInputRef.current.focus();
    }
  };

  const LeadingIcon = useCallback(() => {
    if (!leadingIcon) {
      return;
    }
    const Icon = Assets[leadingIcon];
    return <Icon color={isFocused ? theme.colors.primaryRed : theme.colors.lightGray} />;
  }, [isFocused, leadingIcon, theme.colors.lightGray, theme.colors.primaryRed]);

  const TrailingIcon = useCallback(() => {
    if (!trailingIcon) {
      return;
    }
    const Icon = Assets[trailingIcon];
    return <Icon color={isFocused ? theme.colors.primaryRed : theme.colors.lightGray} />;
  }, [isFocused, theme.colors.lightGray, theme.colors.primaryRed, trailingIcon]);

  const labelAnimation = (type: 'focus' | 'blur') => {
    if (type === 'focus') {
      setIsFocused(true);
      translateY.value -= 10;
      leadingIcon ? (translateX.value -= 7) : (translateX.value -= 6);

      scale.value -= 0.3;
      colorAnimation.value = 1;
    } else {
      setIsFocused(false);
      translateY.value += 10;
      leadingIcon ? (translateX.value += 7) : (translateX.value += 6);
      scale.value += 0.3;
      colorAnimation.value = 0;
    }
  };

  const handleChange = (val: string) => {
    onChangeText(val);
  };

  const animatedStyles = useAnimatedStyle(() => ({
    transform: [
      {translateY: withTiming(translateY.value, labelAnimationProperties)},
      {translateX: withTiming(translateX.value, labelAnimationProperties)},
      {scale: withTiming(scale.value, labelAnimationProperties)},
    ],
  }));

  const colorStyle = useAnimatedStyle(() => {
    return {
      color: withTiming(
        interpolateColor(colorAnimation.value, [0, 1], [theme.colors.lightGray, theme.colors.white], 'RGB'),
        labelAnimationProperties,
      ),
    };
  });

  const handleFocus = () => {
    if (!value) {
      onFocus();
      labelAnimation('focus');
    }
  };

  const handleBlur = () => {
    onBlur();
    if (!value) {
      labelAnimation('blur');
    }
  };

  const onSubmitHandler = () => {
    onSubmit();
  };

  return (
    <View style={[{...styles.container, ...containerStyle}]}>
      <TouchableWithoutFeedback onPress={setRef}>
        <View
          style={[
            styles.innerContainer,
            leadingIcon || trailingIcon ? styles.innerContainerWithIcons : undefined,
            isPassword ? styles.innerContainerPassword : undefined,
            isFocused ? styles.focusedContainer : undefined,
            error ? styles.innerContainerError : undefined,
          ]}>
          {leadingIcon ? (
            <View style={[styles.leadingIconContainer, leadingIconContainerStyle]}>
              <LeadingIcon />
            </View>
          ) : null}

          <TextInput
            ref={ref || textInputRef}
            style={[styles.textInput, textInputStyle]}
            secureTextEntry={isPassword! && !isPasswordVisible}
            cursorColor={theme.colors.white}
            autoCorrect={false}
            autoCapitalize="none"
            value={value}
            maxLength={maxLength}
            onChangeText={handleChange}
            onFocus={handleFocus}
            onBlur={handleBlur}
            returnKeyType={returnKetType}
            onSubmitEditing={onSubmitHandler}
            blurOnSubmit={returnKetType === 'go' ? false : true}
          />
          <Animated.Text
            style={[
              styles.label,
              leadingIcon ? styles.labelWithIcons : undefined,
              animatedStyles,
              colorStyle,
              error ? styles.labelError : undefined,
            ]}>
            {label}
          </Animated.Text>

          {isPassword ? (
            <TouchableOpacity
              style={styles.trailingIconContainer}
              onPress={() => setIsPasswordVisible(!isPasswordVisible)}>
              <Assets.EYE />
            </TouchableOpacity>
          ) : null}

          {trailingIcon ? (
            <TouchableOpacity
              style={[styles.trailingIconContainer, trailingIconIconContainerStyle]}
              onPress={onTrailingIconPress}>
              <TrailingIcon />
            </TouchableOpacity>
          ) : null}
        </View>
      </TouchableWithoutFeedback>
      <ErrorText error={error} />
    </View>
  );
});

export default TextField;
