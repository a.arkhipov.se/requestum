import React, {FC, memo} from 'react';
import {Direction} from './types';
import {StyledSpacer} from './styled';

interface IProps {
  direction?: Direction;
  value?: number;
}

const Spacer: FC<IProps> = ({value = 8, direction = 'horizontal'}) => (
  <StyledSpacer direction={direction} value={value} />
);

export default memo(Spacer);
