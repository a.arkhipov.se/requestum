export type Direction = 'horizontal' | 'vertical';

export type TStyledView = {
  direction?: Direction;
  value?: number;
};
