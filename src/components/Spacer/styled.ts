import styled from 'styled-components/native';
import {TStyledView} from './types';

export const StyledSpacer = styled.View<TStyledView>`
  ${props => (props.direction === 'vertical' ? `margin-bottom: ${props.value}px` : `margin-right: ${props.value}px`)};
`;
