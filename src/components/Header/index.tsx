import React, {FC, ReactElement, memo} from 'react';
import {StyleSheet, Text, TextStyle, TouchableOpacity, View} from 'react-native';

import Icons from '../../assets/svg';
import {useNavigation} from '@react-navigation/core';
import {StackNavigationProp} from '@react-navigation/stack';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {AuthStackNavigatorParamsList} from '../../navigation/types';

interface IProps {
  title?: string;
  children?: ReactElement | null;
  hasBackButton?: boolean;
  isMultiline?: boolean;
  titleStyle?: TextStyle;
  shouldUseSafeArea?: boolean;
}

const Header: FC<IProps> = memo(
  ({title, hasBackButton = true, titleStyle = undefined, isMultiline = false, shouldUseSafeArea = true}) => {
    const navigation = useNavigation<StackNavigationProp<AuthStackNavigatorParamsList>>();

    const {top} = useSafeAreaInsets();

    const paddingTop = shouldUseSafeArea ? top + 30 : 0 + 30;

    const goBackHandler = () => {
      navigation.goBack();
    };

    return (
      <View style={[styles.container, isMultiline ? styles.multiLine : undefined, {paddingTop}]}>
        {hasBackButton ? (
          <TouchableOpacity style={styles.backButton} onPress={goBackHandler}>
            <Icons.LEFT_ARROW />
          </TouchableOpacity>
        ) : null}
        {title ? (
          <View>
            <Text style={[isMultiline ? styles.multiLineText : undefined, titleStyle]}>{title}</Text>
          </View>
        ) : null}
      </View>
    );
  },
);

export default Header;

const styles = StyleSheet.create({
  backButton: {
    width: 48,
    height: 48,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    backgroundColor: 'transparent',
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center',
  },
  multiLine: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    paddingBottom: 8,
  },
  multiLineText: {
    paddingHorizontal: 16,
  },
});
