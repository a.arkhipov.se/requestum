import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    paddingTop: 6,
    // position: 'absolute',
    // bottom: -1,
  },
  text: {
    color: 'red',
    fontSize: 10,
    fontWeight: '400',
    lineHeight: 12,
    bottom: 2,
    left: 16,
  },
  emptyText: {
    color: 'transparent',
    fontSize: 10,
    fontWeight: '400',
    lineHeight: 12,
    bottom: 2,
  },
});
