import React, {FC, memo} from 'react';
import {Text, View} from 'react-native';

import {styles} from './styles';

interface Props {
  error: string | undefined;
}

const ErrorText: FC<Props> = memo(({error}) => {
  return error ? (
    <View style={styles.container}>
      <Text style={styles.text}>{error}</Text>
    </View>
  ) : (
    <View style={styles.container}>
      <Text style={styles.emptyText}> </Text>
    </View>
  );
});

export default ErrorText;
