import styled from 'styled-components/native';
import {TCImageBackground} from './types';

const InnerContainer = styled.ImageBackground<TCImageBackground>`
  flex: 1;
  flex-grow: 1;
  align-self: stretch;
  background-color: ${props => props.theme.darkGray};
  padding-horizontal: 35px;
  justify-content: center;
  align-items: center;
`;

export default InnerContainer;
