import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  textContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonContainer: {
    alignSelf: 'stretch',
  },
});
