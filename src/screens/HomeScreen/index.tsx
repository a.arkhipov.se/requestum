import React from 'react';
import {Image, View} from 'react-native';
import {useTheme} from '@react-navigation/native';

import BackgroundImage from '../../assets/images/hp.png';

import {CustomImageBackground, InnerContainer, Spacer, StyledButton, Text} from '../../components';
import {useAppDispatch, useAppSelector} from '../../store';
import {clearUserData} from '../../store/authSlice';
import {styles} from './styles';

const HomeScreen = () => {
  const dispatch = useAppDispatch();
  const user = useAppSelector(state => state.auth.user);

  const theme = useTheme();

  const handleLogOut = () => {
    dispatch(clearUserData());
  };

  console.log(user);

  const image = Image.resolveAssetSource(BackgroundImage).uri;

  return (
    <CustomImageBackground source={{uri: image}}>
      <InnerContainer>
        <View style={styles.textContainer}>
          <Text text={'You’re loggin in now'} typography="BodyText2" textColor={theme.colors.lightGray} />
          <Spacer direction="vertical" value={8} />
          <Text text={user?.name} typography="Header2" textColor={theme.colors.white} />
          <Spacer direction="vertical" value={50} />
          <Text text={'Now you can see the app content!'} typography="BodyText1" textColor={theme.colors.white} />
        </View>
        <Spacer direction="vertical" value={80} />
        <View style={styles.buttonContainer}>
          <StyledButton type={'solid'} title={'Log out'} color={theme.colors.primaryRed} onPress={handleLogOut} />
        </View>
      </InnerContainer>
    </CustomImageBackground>
  );
};

export default HomeScreen;
