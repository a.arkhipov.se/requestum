import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  logoContainer: {
    flex: 1,
    flexGrow: 2,
    justifyContent: 'center',
  },
  buttonContainer: {
    flex: 1,
    alignSelf: 'stretch',
    justifyContent: 'flex-end',
    paddingBottom: 70,
  },
});
