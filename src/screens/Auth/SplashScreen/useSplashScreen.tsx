import {AuthStackNavigatorParamsList} from '../../../navigation/types';
import {StackNavigationProp} from '@react-navigation/stack/lib/typescript/src/types';
import {useNavigation} from '@react-navigation/native';
import {ROUTES} from '../../../navigation/routes';

export function useSplashScreen() {
  const {navigate} = useNavigation<StackNavigationProp<AuthStackNavigatorParamsList>>();

  const handleSignUp = () => {
    navigate(ROUTES.SIGNUP);
  };

  const handleLogIn = () => {
    navigate(ROUTES.LOGIN);
  };

  return [handleSignUp, handleLogIn];
}
