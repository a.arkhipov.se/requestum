import React from 'react';
import {Image, View} from 'react-native';

import Icons from '../../../assets/svg';

import BackgroundImage from '../../../assets/images/bg.png';

import {styles} from './styles';
import {styledTheme} from '../../../assets';
import {useSplashScreen} from './useSplashScreen';
import {CustomImageBackground, InnerContainer, Spacer, StyledButton} from '../../../components';

const SplashScreen = () => {
  const [handleSignUp, handleLogIn] = useSplashScreen();

  const image = Image.resolveAssetSource(BackgroundImage).uri;

  return (
    <CustomImageBackground source={{uri: image}}>
      <InnerContainer>
        <View style={styles.logoContainer}>
          <Icons.LOGO />
        </View>
        <View style={styles.buttonContainer}>
          <StyledButton title={'Login'} type={'solid'} onPress={handleLogIn} color={styledTheme.color.primaryRed} />
          <Spacer direction="vertical" value={16} />
          <StyledButton title={'Register'} type={'solid'} onPress={handleSignUp} color={styledTheme.color.darkGray} />
        </View>
      </InnerContainer>
    </CustomImageBackground>
  );
};

export default SplashScreen;
