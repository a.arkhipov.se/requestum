import {AuthStackNavigatorParamsList} from '../../../navigation/types';
import {StackNavigationProp} from '@react-navigation/stack/lib/typescript/src/types';
import {useNavigation, useTheme} from '@react-navigation/native';
import {ROUTES} from '../../../navigation/routes';
import {yupResolver} from '@hookform/resolvers/yup';
import {useRef, useEffect} from 'react';
import {useForm} from 'react-hook-form';
import {TextInput} from 'react-native';
import {useQuery} from 'react-query';
import {StyledButton} from '../../../components';
import {fetchUserData} from '../../../services/api/sendRequest';
import {useAppDispatch} from '../../../store';
import {setUserData} from '../../../store/authSlice';
import {signUpSchema} from '../../../utils/validationSchemas';

export function useSignUpScreen() {
  const {navigate} = useNavigation<StackNavigationProp<AuthStackNavigatorParamsList>>();

  const theme = useTheme();

  type StyledHandle = React.ElementRef<typeof StyledButton>;

  const emailInputRef = useRef<TextInput>(null);
  const passwordInputRef = useRef<TextInput>(null);
  const signUpRef = useRef<StyledHandle>(null);

  const dispatch = useAppDispatch();

  const {
    control,
    handleSubmit,
    formState: {errors},
  } = useForm({
    defaultValues: {
      email: '',
      password: '',
    },
    resolver: yupResolver(signUpSchema),
  });

  const {data, refetch, isSuccess} = useQuery({
    queryKey: ['todos'],
    queryFn: () => fetchUserData('https://jsonplaceholder.typicode.com/users/1'),
    refetchOnWindowFocus: false,
    enabled: false,
    cacheTime: 0,
    retry: false,
    refetchOnMount: false,
    retryOnMount: false,
  });

  useEffect(() => {
    if (isSuccess && data) {
      dispatch(setUserData(data));
    }
  });

  const handleEmailSubmitButton = () => {
    passwordInputRef.current?.focus();
  };

  const onSubmit = () => {
    refetch();
  };

  const handleLogIn = () => {
    navigate(ROUTES.LOGIN);
  };

  return [
    emailInputRef,
    passwordInputRef,
    signUpRef,
    theme,
    control,
    errors,
    handleSubmit,
    handleEmailSubmitButton,
    handleLogIn,
    onSubmit,
  ] as const;
}
