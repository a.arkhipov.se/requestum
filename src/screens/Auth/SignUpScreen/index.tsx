import React, {FC} from 'react';
import {Image, View} from 'react-native';

import BackgroundImage from '../../../assets/images/bg.png';

import {
  CustomImageBackground,
  Header,
  InnerContainer,
  Spacer,
  StyledButton,
  Text,
  TextField,
} from '../../../components';
import {Controller} from 'react-hook-form';
import Icons from '../../../assets/svg';
import {useSignUpScreen} from './useSignUpScreen';
import {styles} from './styles';

const SignUpScreen: FC = () => {
  const [
    emailInputRef,
    passwordInputRef,
    signUpRef,
    theme,
    control,
    errors,
    handleSubmit,
    handleEmailSubmitButton,
    handleLogIn,
    onSubmit,
  ] = useSignUpScreen();

  const image = Image.resolveAssetSource(BackgroundImage).uri;

  return (
    <CustomImageBackground source={{uri: image}}>
      <Header />
      <InnerContainer>
        <View style={styles.contentWrapper}>
          <View style={styles.iconContainer}>
            <Icons.LOGIN_ICON />
          </View>
          <View>
            <View style={styles.headerContainer}>
              <Text text={'SIGNUP'} align="center" typography="Header2" textColor={theme.colors.white} />
              <Spacer direction="vertical" value={8} />
              <Text
                text={'Create your email and  password for your account'}
                align="center"
                typography="BodyText2"
                textColor={theme.colors.lightGray}
              />
            </View>
            <Spacer direction="vertical" value={24} />
            <Controller
              control={control}
              name="email"
              render={({field: {onChange, value}}) => (
                <TextField
                  ref={emailInputRef}
                  leadingIcon={'Email'}
                  value={value}
                  onChangeText={onChange}
                  error={errors.email?.message}
                  label="Email"
                  returnKetType="go"
                  onSubmit={handleEmailSubmitButton}
                />
              )}
            />
            <Controller
              control={control}
              name="password"
              render={({field: {onChange, value}}) => (
                <TextField
                  ref={passwordInputRef}
                  leadingIcon={'Lock'}
                  value={value}
                  onChangeText={onChange}
                  isPassword
                  error={errors.password?.message}
                  label="Password"
                />
              )}
            />
            <StyledButton
              ref={signUpRef}
              type={'solid'}
              title={'SignUp'}
              color={theme.colors.primaryRed}
              onPress={handleSubmit(onSubmit, () => signUpRef?.current?.errorHandler())}
            />
          </View>
          <View style={styles.loginContainer}>
            <Text text={'Already have an account? '} textColor={theme.colors.white} typography={'BodyText2'} />
            <StyledButton type={'text'} title={'LogIn'} textColor={theme.colors.primaryRed} onPress={handleLogIn} />
          </View>
        </View>
      </InnerContainer>
    </CustomImageBackground>
  );
};

export default SignUpScreen;
