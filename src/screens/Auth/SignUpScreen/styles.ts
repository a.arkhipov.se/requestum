import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  contentWrapper: {
    flex: 1,
    alignSelf: 'stretch',
    justifyContent: 'space-evenly',
  },
  iconContainer: {
    alignItems: 'flex-end',
    paddingHorizontal: 40,
  },
  headerContainer: {
    alignItems: 'center',
  },
  loginContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
});
