import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  contentWrapper: {
    flex: 1,
    alignSelf: 'stretch',
    justifyContent: 'space-evenly',
  },
  iconContainer: {
    alignItems: 'flex-end',
    paddingHorizontal: 40,
  },
  headerContainer: {
    alignItems: 'center',
  },
  forgotPasswordContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  loginContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
});
