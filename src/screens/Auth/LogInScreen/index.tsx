import React, {FC} from 'react';
import {Image, View} from 'react-native';

import BackgroundImage from '../../../assets/images/bg.png';

import {
  CustomImageBackground,
  Header,
  InnerContainer,
  Spacer,
  StyledButton,
  Text,
  TextField,
} from '../../../components';
import {Controller} from 'react-hook-form';
import Icons from '../../../assets/svg';
import {styles} from './styles';
import {useLogInScreen} from './useLogInScreen';

const LogInScreen: FC = () => {
  const [
    emailInputRef,
    passwordInputRef,
    signUpRef,
    theme,
    control,
    errors,
    handleSubmit,
    handleEmailSubmitButton,
    handleSignUp,
    onSubmit,
  ] = useLogInScreen();

  const image = Image.resolveAssetSource(BackgroundImage).uri;

  return (
    <CustomImageBackground source={{uri: image}}>
      <Header />
      <InnerContainer>
        <View style={styles.contentWrapper}>
          <View style={styles.iconContainer}>
            <Icons.LOGIN_ICON />
          </View>
          <View>
            <View style={styles.headerContainer}>
              <Text text={'LOGIN'} align="center" typography="Header2" textColor={theme.colors.white} />
              <Spacer direction="vertical" value={24} />
            </View>
            <Spacer direction="vertical" value={24} />
            <Controller
              control={control}
              name="email"
              render={({field: {onChange, value}}) => (
                <TextField
                  ref={emailInputRef}
                  leadingIcon={'Email'}
                  value={value}
                  onChangeText={onChange}
                  error={errors.email?.message}
                  label="Email"
                  returnKetType="go"
                  onSubmit={handleEmailSubmitButton}
                />
              )}
            />
            <Controller
              control={control}
              name="password"
              render={({field: {onChange, value}}) => (
                <TextField
                  ref={passwordInputRef}
                  leadingIcon={'Lock'}
                  value={value}
                  onChangeText={onChange}
                  isPassword
                  error={errors.password?.message}
                  label="Password"
                />
              )}
            />
            <View style={styles.forgotPasswordContainer}>
              <StyledButton type={'text'} title={'Forgot password'} textColor={theme.colors.primaryRed} />
            </View>
            <StyledButton
              ref={signUpRef}
              type={'solid'}
              title={'Login'}
              color={theme.colors.primaryRed}
              onPress={handleSubmit(onSubmit, () => signUpRef?.current?.errorHandler())}
            />
          </View>
          <View style={styles.loginContainer}>
            <Text text={"Don't have an account?  "} textColor={theme.colors.white} typography={'BodyText2'} />
            <StyledButton type={'text'} title={'Sign up'} textColor={theme.colors.primaryRed} onPress={handleSignUp} />
          </View>
        </View>
      </InnerContainer>
    </CustomImageBackground>
  );
};

export default LogInScreen;
