import AsyncStorage from '@react-native-async-storage/async-storage';
import {persistReducer, persistStore} from 'redux-persist';
import rootReducer from './rootReducer';
import {configureStore, createAction} from '@reduxjs/toolkit';
import {TypedUseSelectorHook, useDispatch, useSelector} from 'react-redux';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['auth'],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const configureCustomStore = () => {
  const store = configureStore({
    reducer: persistedReducer,
    middleware: getDefaultMiddleware =>
      __DEV__
        ? getDefaultMiddleware({serializableCheck: false})
        : getDefaultMiddleware({
            serializableCheck: false,
          }),
  });

  return {store};
};

const {store} = configureCustomStore();

const persistor = persistStore(store);

export const clearAll = createAction('test');

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

const useAppDispatch: () => AppDispatch = useDispatch;
const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;

export {store, persistor, useAppDispatch, useAppSelector};
