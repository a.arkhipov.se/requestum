import AsyncStorage from '@react-native-async-storage/async-storage';
import authSlice from './authSlice';
import {combineReducers} from '@reduxjs/toolkit';
import {persistReducer} from 'redux-persist';

const authPersistConfig = {
  key: 'auth',
  storage: AsyncStorage,
};

export const rootReducer = combineReducers({
  auth: persistReducer(authPersistConfig, authSlice),
});

export type TRootState = ReturnType<typeof rootReducer>;

export default rootReducer;
