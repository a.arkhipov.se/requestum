import * as yup from 'yup';

export const signUpSchema = yup
  .object({
    email: yup
      .string()
      .email('Enter correct email')
      .min(8, 'Min length 8 symbols')
      .max(30, 'Max length 30 symbols')
      .required(),
    password: yup.string().min(8, 'Min length 8 symbols').max(30, 'Max length 30 symbols').required(),
  })
  .required();
