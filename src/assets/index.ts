import {Dimensions} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

const windowHeight = Dimensions.get('window').height;

export const styledTheme = {
  color: {
    primaryRed: '#EB0057',
    lightGray: '#818181',
    darkGray: '#262727',
    white: '#FFFFFF',
  },
  fonts: {
    'sans-serif': 'sans-serif',
    montserrat: 'Montserrat',
    montserratThin: 'Montserrat-Thin',
    montserratLight: 'Montserrat-Light',
  },
  size: {
    11: RFValue(11, windowHeight),
    12: RFValue(12, windowHeight),
    14: RFValue(14, windowHeight),
    16: RFValue(16, windowHeight),
    18: RFValue(18, windowHeight),
    20: RFValue(20, windowHeight),
    22: RFValue(22, windowHeight),
    24: RFValue(24, windowHeight),
    26: RFValue(26, windowHeight),
    28: RFValue(28, windowHeight),
    30: RFValue(30, windowHeight),
    32: RFValue(32, windowHeight),
    34: RFValue(34, windowHeight),
    36: RFValue(36, windowHeight),
    38: RFValue(38, windowHeight),
    40: RFValue(40, windowHeight),
  },
};
