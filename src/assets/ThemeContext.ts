import {createContext} from 'react';
import {Dimensions, StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export const theme = {
  dark: false,
  colors: {
    textColor: '#FFFFFF',
    primaryRed: '#EB0057',
    lightGray: '#818181',
    darkGray: '#262727',
    white: '#FFFFFF',
    primary: 'rgb(0, 122, 255)',
    background: '#262727',
    card: 'rgb(255, 255, 255)',
    text: 'rgb(28, 28, 30)',
    border: 'rgb(216, 216, 216)',
    notification: 'rgb(255, 59, 48)',
  },
};
const windowHeight = Dimensions.get('window').height;

export type Theme = typeof theme;

export const fonts = {
  'sans-serif': 'sans-serif',
  montserrat: 'Montserrat',
  montserratThin: 'Montserrat-Thin',
  montserratLight: 'Montserrat-Light',
};

const size = {
  11: RFValue(11, windowHeight),
  12: RFValue(12, windowHeight),
  14: RFValue(14, windowHeight),
  16: RFValue(16, windowHeight),
  18: RFValue(18, windowHeight),
  20: RFValue(20, windowHeight),
  22: RFValue(22, windowHeight),
  24: RFValue(24, windowHeight),
  26: RFValue(26, windowHeight),
  28: RFValue(28, windowHeight),
  30: RFValue(30, windowHeight),
  32: RFValue(32, windowHeight),
  34: RFValue(34, windowHeight),
  36: RFValue(36, windowHeight),
  38: RFValue(38, windowHeight),
  40: RFValue(40, windowHeight),
};

export const typography = StyleSheet.create({
  Header2: {
    fontSize: size[18],
    lineHeight: size[20],
    fontFamily: fonts.montserrat,
    fontWeight: '600',
    textTransform: 'uppercase',
  },
  ButtonText: {
    fontSize: size[14],
    lineHeight: size[16],
    fontFamily: fonts.montserrat,
  },
  BodyText2: {
    fontSize: 12,
    lineHeight: 14,
    fontFamily: fonts.montserratLight,
    fontWeight: '300',
  },
  BodyText1: {
    fontSize: size[16],
    lineHeight: size[18],
    fontFamily: fonts.montserrat,
    fontWeight: '500',
    fontStyle: 'normal',
  },
});

export type TypographyKeys = keyof typeof typography;
export type Typography = typeof typography;

export type IContextTheme = typeof theme;

export const ThemeContext = createContext<{
  theme: IContextTheme;
  typography: Typography;
}>({theme: theme, typography});
