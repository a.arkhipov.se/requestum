import React, {FC} from 'react';
import {ThemeContext, theme as appTheme, typography} from './ThemeContext';
import {useColorScheme} from 'react-native';

export const ThemeProvider: FC<{children: React.ReactNode}> = ({children}) => {
  const currentTheme = useColorScheme();

  const theme = currentTheme === 'dark' ? appTheme : appTheme;

  return <ThemeContext.Provider value={{theme, typography}}>{children}</ThemeContext.Provider>;
};
