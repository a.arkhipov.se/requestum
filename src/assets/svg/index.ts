import Logo from './Logo.svg';
import LeftArrow from './LeftArrow.svg';
import Eye from './Eye.svg';
import Email from './Email.svg';
import Lock from './Lock.svg';
import LoginIcon from './LoginIcon.svg';

export const Icons = {
  LOGO: Logo,
  LEFT_ARROW: LeftArrow,
  EYE: Eye,
  Email: Email,
  Lock: Lock,
  LOGIN_ICON: LoginIcon,
};

export type IconType = keyof typeof Icons;

export default Icons;
