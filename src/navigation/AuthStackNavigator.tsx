import React from 'react';

import {createStackNavigator} from '@react-navigation/stack';

import {ROUTES} from './routes';

import SplashScreen from '../screens/Auth/SplashScreen';
import SignUpScreen from '../screens/Auth/SignUpScreen';
import LogInScreen from '../screens/Auth/LogInScreen';

const AuthStack = createStackNavigator();

const AuthStackNavigator = () => {
  return (
    <AuthStack.Navigator
      screenOptions={{
        headerShown: false,
        headerTransparent: true,
      }}
      initialRouteName={ROUTES.SPLASH}>
      <AuthStack.Screen component={SplashScreen} name={ROUTES.SPLASH} />
      <AuthStack.Screen component={LogInScreen} name={ROUTES.LOGIN} />
      <AuthStack.Screen component={SignUpScreen} name={ROUTES.SIGNUP} />
    </AuthStack.Navigator>
  );
};

export default AuthStackNavigator;
