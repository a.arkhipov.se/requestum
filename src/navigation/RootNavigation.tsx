import React from 'react';
import {KeyboardAvoidingView, Platform, StatusBar, StyleSheet} from 'react-native';
import AuthStackNavigator from './AuthStackNavigator';
import {useAppSelector} from '../store';
import StackNavigator from './StackNavigator';

const RootNavigation = () => {
  const {user} = useAppSelector(state => state.auth);

  return (
    <KeyboardAvoidingView style={styles.wrapper} behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
      <StatusBar
        animated={true}
        barStyle={'light-content'}
        backgroundColor="transparent"
        translucent={true}
        hidden={false}
      />
      {user ? <StackNavigator /> : <AuthStackNavigator />}
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
});

export default RootNavigation;
