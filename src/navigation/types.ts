// import { StackNavigationProp } from '@react-navigation/stack';
import {ROUTES} from './routes';
import {NavigationProp} from '@react-navigation/native';

export type AuthStackNavigatorParamsList = {
  [ROUTES.SPLASH]: undefined;
  [ROUTES.LOGIN]: undefined;
  [ROUTES.SIGNUP]: undefined;
};

export type AuthStackNavigation = NavigationProp<AuthStackNavigatorParamsList>;
