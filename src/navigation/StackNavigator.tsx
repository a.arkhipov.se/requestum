import React from 'react';

import {createStackNavigator} from '@react-navigation/stack';

import {ROUTES} from './routes';

import HomeScreen from '../screens/HomeScreen';

const AuthStack = createStackNavigator();

const StackNavigator = () => {
  return (
    <AuthStack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName={ROUTES.HOME}>
      <AuthStack.Screen component={HomeScreen} name={ROUTES.HOME} />
    </AuthStack.Navigator>
  );
};

export default StackNavigator;
