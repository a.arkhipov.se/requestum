export enum ROUTES {
  SPLASH = 'Splash',
  LOGIN = 'LogIn',
  SIGNUP = 'SignUp',
  HOME = 'Home',
}
