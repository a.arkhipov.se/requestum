import axios from 'axios';
import {User} from '../../store/authSlice';

export const fetchUserData = async (url: string): Promise<User | undefined> => {
  try {
    const response = await axios.get(url).then(res => res.data);
    return response;
  } catch (error) {
    console.warn('Axios GET Request ERROR: ', error);
  }
};
