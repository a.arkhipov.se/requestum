import type {Theme} from './src/assets/ThemeContext';

declare module '@react-navigation/native' {
  export function useTheme(): Theme;
}
